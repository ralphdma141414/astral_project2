using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundCreate : MonoBehaviour
{
    [HideInInspector]
    public AudioSource sound;
    public AudioClip soundObj;
    [HideInInspector]
    public int count;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        sound = GetComponent<AudioSource>();
        sound.clip = soundObj;
    }

    public void Sounding()
    {
        if (count == 0)
        {
            sound.Play();
            count++;
        }
    }

}
