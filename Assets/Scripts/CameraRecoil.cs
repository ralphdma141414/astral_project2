using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRecoil : MonoBehaviour
{
    public Transform cam, rotationOrigin;
    private RaycastHit hit;
    [HideInInspector]
    public Vector3 camOff;
    [HideInInspector]
    public bool dead;

    // Start is called before the first frame update
    void Start()
    {
        camOff = cam.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (dead == false)
        {
            Recoiling();
        }
    }

    void Recoiling()
    {
        if (cam.localPosition.z > 0)
        {
            /*Vector3 ver = cam.localPosition;
            ver.z = -0.0001f;
            cam.localPosition = ver;*/
        }
        LayerMask mask = LayerMask.GetMask("FloorAndWall");
        if (Physics.Linecast(transform.position, transform.position + rotationOrigin.transform.localRotation * camOff, out hit, mask))
        {
            cam.localPosition = new Vector3(0, 0, -Vector3.Distance(transform.position, hit.point));
        }
        else
        {
            cam.localPosition = Vector3.Lerp(cam.localPosition, camOff, Time.deltaTime);
        }
    }
}