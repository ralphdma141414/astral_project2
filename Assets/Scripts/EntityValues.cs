using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityValues : MonoBehaviour
{
    public float currentLife; //Vida actual.
    public float deadTime;
    public GameObject fade;
    public bool isPlayer; //Si esta variable est� activa, la entidad con este script ser� considerada Player, si no lo est�, ser� Enemigo.
    [HideInInspector]
    public bool stunned, damaged = false;
    public Animator anim;
    void Start()
    {

    }
    void Update()
    {
        Die();
    }
    void OnTriggerEnter(Collider other)
    {
        if (isPlayer) //Si este es el jugador toca...
        {
            if (other.gameObject.CompareTag("Enemy bullet")) //un proyectil enemigo, pierde vida. (los n�meros de abajo son valores de prueba)
            {
                currentLife -= 5; //pierde: 5 o la variable del da�o de proyectil enemigo.
            }
            else if (other.gameObject.CompareTag("Enemy explotion"))
            {
                currentLife -= 40; //pierde: 40 o variable del da�o de explosi�n enemiga.
                stunned = true;
            }
            else if (other.gameObject.CompareTag("Enemy thunder"))
            {
                currentLife -= 10; //pierde: 10 o variable del da�o del empuje del enemiga.
                stunned = true;
            }
            else if (other.gameObject.CompareTag("DeadFall"))
            {
                currentLife = -10; //Pierde toda la vida
            }
        }
        else if (other.gameObject.CompareTag("Allied bullet") && !isPlayer) //Si este es un enemigo y es atacado por un proyectil del jugador, pierde vida.
        {
            currentLife -= 10; //pierde: 10 o variable de da�o de proyectil de jugador.
            damaged = true;
        }
        
    }
    void Die()
    {
        if (isPlayer)
        {
            if (currentLife <= 0) //si su vida llega a 0, muere.
            {
                fade.SetActive(true);
                deadTime += Time.deltaTime;
                if (deadTime >= 3f)
                {
                    anim.SetBool("Dead", true);
                }
            }
        }
        else
        {
            if (currentLife <= 0) //si su vida llega a 0, muere.
            {
                deadTime += Time.deltaTime;
                if (deadTime >= 3f)
                {
                    ObjectDisappear();
                }
            }
        }

    }
    public void ObjectDisappear()
    {
        Destroy(gameObject);
    }
}