using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundAlter : MonoBehaviour
{
    public Slider volume;
    private GameObject gaming;
    private TheCore lor;
    // Start is called before the first frame update
    void Start()
    {
        volume = GetComponent<Slider>();
        gaming = GameObject.FindGameObjectWithTag("The Core");
        lor = gaming.GetComponent<TheCore>();
        volume.value = lor.volume;
    }

    // Update is called once per frame
    void Update()
    {
        SoundMake();
    }

    void SoundMake()
    {
        lor.volume = volume.value;
    }
}
