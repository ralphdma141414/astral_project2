using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Variables
    private Rigidbody rigid;
    public float speed, dashSpeed;
    public float sensible;
    private float rotation, newRotation, securityAngle, dashTime = 0, stunTime = 0, currentSense, see, saw, mouseLock, count = 0;
    [HideInInspector]
    public int direction, legion; // Direcciones X y Z
    private bool dashing = false, stayed = false;
    [HideInInspector]
    public float dashCD = 5;
    public EntityValues stats;
    public CameraRecoil camer;
    public GameObject vertex, follow, cameraObject, newVertex, model, currentFollow;
    private Vector3 lookDash;
    public CameraRecoil camDead;
    public Recoiler coDead;
    public Pause pauseDead;

    // Start is called before the first frame update
    void Start()
    {
        currentSense = sensible;
        rigid = GetComponent<Rigidbody>();
        cameraObject.transform.localEulerAngles = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (stats.currentLife > 0)
        {
            if (stats.stunned == false)
            {
                Dasher();
                if (dashing == false)
                {
                    Move();
                    Directioning();
                }
                else
                {
                    Dash();
                }
            }
            else
            {
                Stunned();
            }
            Camera();
            Model();
        }
        else
        {
            Deadcam();
        }
    }

    void Move()
    {
        rigid.useGravity = true;
        float h = Input.GetAxisRaw("Horizontal") * Time.deltaTime;
        float v = Input.GetAxisRaw("Vertical") * Time.deltaTime;
        // Movimiento de jugador y tambien las direcciones que mira el jugador
        if (h > 0)
        {
            direction = -1;
        }
        else if (h < 0)
        {
            direction = 1;
        }
        else
        {
            direction = 0;
        }
        if (v > 0)
        {
            legion = 1;
        }
        else if (v < 0)
        {
            legion = -1;
        }
        else
        {
            legion = 0;
        }
    }

    void Dash()
    {
        dashTime += Time.deltaTime;
        Vector3 vel = rigid.velocity;
        vel = lookDash * (dashSpeed);
        rigid.velocity = vel;
        rigid.useGravity = false;
        if (count == 0)
        {
            /*Vector3 cameraFun = camer.cam.position;
            cameraFun.z = camer.cam.position.z * 2;
            camer.cam.position = cameraFun;*/
            camer.camOff.z *= 2;
            count++;
        }
        vertex.transform.localEulerAngles = new Vector3(0, rotation + newRotation, 0);
        if (dashTime >= 0.3f)
        {
            dashing = false;
            dashTime = 0;
            /*Vector3 cameraFun = camer.cam.position;
            cameraFun.z = camer.cam.position.z / 2;
            camer.cam.position = cameraFun;*/
            camer.camOff.z /= 2;
            count = 0;
        }
    }

    void Directioning()
    {

        if (direction != 0 || legion != 0)
        {
            if (direction == 0)
            {
                rotation = legion - 1;
                rotation = -90 * rotation;
            }
            else if (legion == 0)
            {
                rotation = -direction * 90;
            }
            else
            {
                rotation = 90 - 45 * legion;
                rotation *= -direction;
            }
            vertex.transform.localEulerAngles = new Vector3(0, rotation + newRotation, 0);
            // Los variables rotation y newRotation estaran en parte con los angulos del jugador
            Vector3 vel = rigid.velocity;

            //El follow es un gameobject, por lo que el jugador lo sigue
            vel.x = ((follow.transform.position.x - transform.position.x) / 10) * speed;
            vel.z = ((follow.transform.position.z - transform.position.z) / 10) * speed;
            rigid.velocity = vel;
        }
        else
        {
            Vector3 vel = rigid.velocity;
            vel.x = 0;
            vel.z = 0;
            rigid.velocity = vel;
        }
    }

    void Camera()
    {

        float mouseX = Input.GetAxis("Mouse X") * sensible * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensible * Time.deltaTime;
        if (cameraObject.transform.localEulerAngles.x >= 180)
        {
            see = cameraObject.transform.localEulerAngles.x-360;
        }
        else
        {
            see = cameraObject.transform.localEulerAngles.x;
        }
        saw = cameraObject.transform.localEulerAngles.z;

        if (see >= -90 && see <= 90)
        {
            if (mouseX != 0 || mouseY != 0)
            {
                if ((see >= 80f && mouseY < 0) || (see <= -80f && mouseY > 0))
                {
                    mouseLock = 0;
                    cameraObject.transform.eulerAngles = new Vector3(cameraObject.transform.eulerAngles.x, cameraObject.transform.eulerAngles.y, 0);
                }
                else
                {
                    mouseLock = 1;
                }
                newRotation += mouseX;
                newVertex.transform.localEulerAngles = new Vector3(0, newRotation, 0);
                securityAngle += mouseY;
                cameraObject.transform.eulerAngles += new Vector3(-mouseY * mouseLock, mouseX, 0);
            }
        }
        else if (see > 90)
        {
            mouseLock = 0;
            cameraObject.transform.eulerAngles = new Vector3(90, cameraObject.transform.eulerAngles.y, 0);
        }
        else if (see < -90)
        {
            mouseLock = 0;
            cameraObject.transform.eulerAngles = new Vector3(-90, cameraObject.transform.eulerAngles.y, 0);
        }

    }

    void Dasher()
    {
        dashCD += Time.deltaTime;
        if (dashCD >= 5)
        {
            dashCD = 5;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && dashing == false && dashCD >= 5 && (direction != 0 || legion != 0))
        {
            dashing = true;
            stayed = false;
            lookDash = (follow.transform.position - transform.position).normalized;
            dashCD = 0;
        }
        else if(Input.GetKeyDown(KeyCode.LeftShift) && dashing == false && dashCD >= 5 && (direction == 0 && legion == 0))
        {
            dashing = true;
            stayed = true;
            lookDash = (currentFollow.transform.position - transform.position).normalized;
            dashCD = 0;
        }
    }

    void Model()
    {
        if (dashing)
        {
            if (stayed)
            {
                model.transform.localEulerAngles = newVertex.transform.localEulerAngles;

            }
            else
            {
                model.transform.localEulerAngles = vertex.transform.localEulerAngles;
            }
        }
        else
        {
            model.transform.localEulerAngles = newVertex.transform.localEulerAngles;
        }
    }
    void Stunned()
    {
        stunTime += Time.deltaTime;
        if (stunTime >= 1)
        {
            stats.stunned = false;
            stunTime = 0;
        }
    }
    void Deadcam()
    {
        camDead.dead = true;
        coDead.dead = true;
        pauseDead.dead = true;
        rigid.velocity = new Vector3(0, rigid.velocity.y, 0);
        cameraObject.transform.localEulerAngles = new Vector3(90, 0, 0);
        coDead.transform.localPosition = new Vector3(0, 0, 0);
    }
}
