﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinerBot : MonoBehaviour
{
    public Rigidbody rigid;
    public GameObject target, temporal, explode, soundExplode, particle;
    [HideInInspector]
    public bool spawned;
    public EntityValues alerte;
    private bool active=false;
    private float rotation;
    private Vector3 look;
    [HideInInspector]
    public Detector deter, touch;
    public EnemyRecoiler looker;
    public SoundCreate sound;
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        target = GameObject.FindGameObjectWithTag("Player head");
    }

    // Update is called once per frame
    void Update()
    {
        if (alerte.currentLife > 0)
        {
            if (active == false)
            {
                Sleeping();
            }
            else if (target != null && active)
            {
                Looking();
                Chasing();
            }
        }
        else
        {
            Explode();
        }
    }

    void Sleeping()
    {
        if (((deter.dosto || alerte.damaged == true) && looker.detect) || spawned)
        {
            active = true;
            rigid.useGravity = false;
            Destroy(temporal);
            sound.Sounding();
            Debug.Log("Tin tin");
        }
    }

    void Looking()
    {
        look = (target.transform.position - transform.position).normalized;
        rotation = Mathf.Atan2(look.x, look.z);
        rotation = rotation * (180 / Mathf.PI);
        transform.localEulerAngles = new Vector3(0, rotation, 0);
    }

    void Chasing()
    {
        rigid.velocity = look*3;
        if (touch.dosto)
        {
            alerte.currentLife = 0;
        }
    }
    void Explode()
    {
        explode.transform.position = transform.position;
        soundExplode.transform.position = transform.position;
        Instantiate(explode);
        GameObject obj = Instantiate(particle);
        obj.transform.position = transform.position;
        Instantiate(soundExplode);
        Destroy(gameObject);
    }
    void VolumeRate()
    {
        if (looker.detect)
        {
            sound.sound.volume = 1f;
        }
        else
        {
            sound.sound.volume = 0.5f;
        }
    }
}
