using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateBar : MonoBehaviour
{
    public EntityValues playerValues;
    public PlayerMovement playerMovement;
    public Image lifeBar;
    public Image dashBar;
    //public Scrollbar staminaBar; futura barra de stamina
    void Start()
    {

    }
    void Update()
    {
        lifeBar.fillAmount = playerValues.currentLife / 100;
        dashBar.fillAmount = playerMovement.dashCD /5;
    }
}
