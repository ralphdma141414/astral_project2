using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRecoiler : MonoBehaviour
{
    public Transform rotationOrigin;
    private RaycastHit hitWall, hitEnemy;
    private Vector3 look;
    private float tryWall, tryEnemy, rotationY, rotationX, beginRot;
    private GameObject target;
    [HideInInspector]
    public bool dead, detect;

    // Start is called before the first frame update
    void Start()
    {
        beginRot = rotationOrigin.eulerAngles.y;
        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (dead == false)
        {
            LookToe();
            Recoiling();
        }
    }
    void LookToe()
    {
        transform.LookAt(target.transform);
    }
    void Recoiling()
    {
        LayerMask mask = LayerMask.GetMask("FloorAndWall");
        LayerMask enemyLook = LayerMask.GetMask("Player");
        tryEnemy = Vector3.Distance(transform.position, target.transform.position);
        if (Physics.Linecast(transform.position, target.transform.position, out hitWall, mask))
        {
            tryWall = Vector3.Distance(transform.position, hitWall.point);
        }
        else
        {
            tryWall = 1000000;
        }
        if (tryWall <= tryEnemy)
        {
            detect = false;
        }
        else if (tryWall > tryEnemy)
        {
            detect = true;
        }
    }
}

