using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
  
    //Esto es para ir a cualquier escena, solo se debe cambiar el texto por 
    //el nombre de la escena sonde se desea ir
    public void CambioEscena(string name)
    {
        SceneManager.LoadScene(name);
    }
    //Esto es para salir aunque no pasa nada por que estamos en unity,pero en el compilado si funciona
    public void Salir()
    {
        Application.Quit();
    }
}

