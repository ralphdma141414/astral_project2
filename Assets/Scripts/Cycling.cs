using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cycling : MonoBehaviour
{
    public void Open(GameObject opening)
    {
        opening.SetActive(true);
    }
    //Esto es para salir aunque no pasa nada por que estamos en unity,pero en el compilado si funciona
    public void Close(GameObject closing)
    {
        closing.SetActive(false);
    }
}
