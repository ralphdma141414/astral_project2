using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAlive : MonoBehaviour
{
    private int count = 0;
    [HideInInspector]
    public float sers;
    public bool alive, nonPlaying, wait, changeSense;
    private GameObject player;
    private PlayerMovement sors;
    void Update()
    {
        See();
    }
    void See()
    {
        if (changeSense)
        {
            sors.sensible = sers;
        }
    }
    public void FindPlayer()
    {
        if (count == 0)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            sors = player.GetComponent<PlayerMovement>();
            count++;
            changeSense = true;
            alive = true;
            wait = false;
        }
        if (player == null && nonPlaying == false && wait == false)
        {
            count = 0;
            alive = false;
        }
    }
    public void Changing()
    {
        wait = true;
        count = 0;
        player = null;
    }
}
