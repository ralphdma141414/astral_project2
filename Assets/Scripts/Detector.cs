﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    public bool dosto;
    public string tagging;
   
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(tagging))
        {
            dosto = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(tagging))
        {
            dosto = false;
        }
    }
}
