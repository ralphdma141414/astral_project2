using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highlighter : MonoBehaviour
{
    public Image roll;
    public bool colorly, pret;
    public GameObject sond;
    public SoundCreate soundly;

    void Start()
    {

        pret = true;
        colorly = true;
        roll = GetComponent<Image>();
        sond = GameObject.FindWithTag("CrossSound");
        soundly = sond.GetComponent<SoundCreate>();
    }
    void Update()
    {
        Booltify();
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            soundly.Sounding();
            colorly = true;
            pret = false;
            soundly.count = 0;
        }
        else
        {
            colorly = false;
        }
    }
    void OnMouseExit()
    {
        colorly = true;
    }
    public void Come()
    {
        pret = true;
        colorly = true;
    }
    void Booltify()
    {
        if (colorly)
        {
            roll.color = Color.black;
        }
        else if (pret)
        {
            roll.color = Color.white;
        }

    }
}
