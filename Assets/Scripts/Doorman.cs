using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doorman : MonoBehaviour
{
    public bool done = false, opening = false;
    private float timer;
    public SoundCreate sound;
    public GameObject gate;
    void Start()
    {
    }
    public void Opening()
    {
        done = true;
    }
    void Update()
    {
        Opener();
    }

    void Opener()
    {
        if (done == true)
        {
            sound.Sounding();
            timer += Time.deltaTime;
            if (timer <= 0.25)
            {
                opening = true;
            }
            else
            {
                opening = false;
                Destroy(gate);
            }
        }
    }
}
