﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Pause : MonoBehaviour
{
    public bool paused = false;
    public ColorText texte;
    public GameObject pauseObject;
    public Highlighter[] tod;
    [HideInInspector]
    public bool dead;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (dead == false)
        {
            Pausing();
        }
    }

    void Pausing()
    {
        if (Input.GetKeyDown(KeyCode.Escape)|| Input.GetKeyDown(KeyCode.P))
        {
            if (paused == false)
            {
                paused = true;
                for(int i = 0; i < tod.Length; i++)
                {
                    tod[i].Come();
                }
            }
        }
        if (paused == true)
        {
            Time.timeScale = 0;
            pauseObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            texte.ColorRevert();
            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1;
            pauseObject.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

        }
    }

    public void PressResume()
    {
        if (paused == true)
        {
            paused = false;
        }
    }
}