using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassDrop : MonoBehaviour
{
    private Rigidbody rigid;
    [HideInInspector]
    public bool broken = false;
    public GlassDrop glass;
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Looken();
    }

    void Looken()
    {
        if (broken == true)
        {
            rigid.constraints = RigidbodyConstraints.None;
            rigid.useGravity = true;
            glass.broken = true;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Allied bullet"))
        {
            broken = true;
        }
    }
}
