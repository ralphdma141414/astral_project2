using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartLose : MonoBehaviour
{
    public GameObject core;
    public TheCore coring;
    private int count=0;
    // Start is called before the first frame update
    void Update()
    {
        if (count == 0)
        {
            core = GameObject.FindGameObjectWithTag("The Core");
            coring = core.GetComponent<TheCore>();
            count++;
        }
    }
    public void Choose()
    {
        coring.Restarting();
    }
}
