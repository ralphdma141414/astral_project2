using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Doorman doorem;
    private Vector3 look;
    public GameObject direction;
    private Rigidbody rigid;
    void Start()
    {
        look = (direction.transform.position - transform.position).normalized;
        rigid = GetComponent<Rigidbody>();
    }
    void Update()
    {
        Opener();
    }
    void Opener()
    {
        if (doorem.opening == true)
        {
            rigid.velocity = look*4;
        }
    }
}
