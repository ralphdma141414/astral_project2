using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossInteractive : MonoBehaviour
{
    public bool available, once;
    public Boss battle;
    public SoundCreate sound;
    public Material emiss;

    void Start()
    {
        once = true;
        emiss.EnableKeyword("_EMISSION");
        emiss.SetColor("_EmissionColor", new Color(255, 0, 0) * 0.015f);
    }
    void Update()
    {
        Pressing();
    }

    void Pressing()
    {
        if (available && once && Input.GetKeyDown(KeyCode.E))
        {
            once = false;
            battle.buttonPress++;
            sound.Sounding();
            emiss.SetColor("_EmissionColor", new Color(0, 255, 0) * 0.017f);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Interect"))
        {
            available = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Interect"))
        {
            available = false;
        }
    }
}
