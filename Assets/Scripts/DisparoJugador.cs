using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoJugador : MonoBehaviour
{

    private float  timer = 0;
    public GameObject fina,dotp, bullet, rotat;
    public EntityValues health;
    public Pause pausing;
    private Vector3 look;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pausing.paused == false && health.currentLife > 0 && health.stunned == false)
        {
            Looking();
            Shooting();
        }
    }

    void Looking()
    {
        look = (fina.transform.position - dotp.transform.position).normalized;
       
    }

    void Shooting()
    {
        timer += Time.deltaTime;
        if (Input.GetMouseButton(0) && timer >= 0.25)
        {
            bullet.transform.position = dotp.transform.position;
            bullet.transform.eulerAngles = rotat.transform.localEulerAngles;
            bullet.GetComponent<Bullet>().vel = look*10;
            bullet.GetComponent<LifeSpan>().maxTimer = 1.5f;
            bullet.tag = "Allied bullet";
            Instantiate(bullet);
            timer = 0;
        }
    }
}
