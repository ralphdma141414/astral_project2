using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixerRecoiler : MonoBehaviour
{
    public Transform cam, rotationOrigin;
    private RaycastHit hit;
    public Vector3 camOff;
    // Start is called before the first frame update
    void Start()
    {
        camOff = cam.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        Recoiling();
    }

    void Recoiling()
    {
        LayerMask mask = LayerMask.GetMask("FloorAndWall");
        if ((Physics.Linecast(transform.position, transform.position + rotationOrigin.transform.localRotation * camOff, out hit, mask)))
        {
            cam.localPosition = new Vector3(Vector3.Distance(transform.position, hit.point), cam.localPosition.y, 0);
        }
        else
        {
            cam.localPosition = Vector3.Lerp(cam.localPosition, camOff, Time.deltaTime);
        }
    }
}