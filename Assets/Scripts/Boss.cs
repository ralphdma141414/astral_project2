using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    public int buttonPress = 0, spawnTries, timeling;
    public float battleTime, realTime;
    public GameObject[] miners, melees;
    public GameObject minerBot, meleeBot;
    public AudioClip bossMusic;
    public bool active = false;
    private GameObject core, target;
    private TheCore coring;
    private Text texter;
    public Doorman finalDoor;

    // Start is called before the first frame update
    void Start()
    {
        core = GameObject.FindGameObjectWithTag("The Core");
        coring = core.GetComponent<TheCore>();
        target = GameObject.FindGameObjectWithTag("Time");
        texter = target.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Preparing();
        if (active == true)
        {
            Battle();
            OpenDoor();
        }
    }
    void Preparing()
    {
        if (buttonPress == 2)
        {
            coring.musique.Stop();
            coring.musique.clip = bossMusic;
            battleTime = 20;
            active = true;
            spawnTries--;
            coring.musique.Play();
            buttonPress++;
        }
    }
    void Battle()
    {
        texter.enabled = true;
        battleTime += Time.deltaTime;
        if (battleTime >= 20)
        {
            spawnTries++;
            for (int i = 0; i < miners.Length; i++)
            {
                minerBot.transform.position = miners[i].transform.position;
                minerBot.GetComponent<MinerBot>().spawned = true;
                Instantiate(minerBot);
                minerBot.GetComponent<MinerBot>().spawned = false;
            }
            for (int i = 0; i < melees.Length; i++)
            {
                meleeBot.transform.position = melees[i].transform.position;
                meleeBot.GetComponent<MeleeBot>().spawned = true;
                Instantiate(meleeBot);
                meleeBot.GetComponent<MeleeBot>().spawned = false;
            }
            battleTime -= 20;
        }
    }
    void OpenDoor()
    {
        if (timeling <= 0)
        {
            finalDoor.Opening();
        }
        else
        {
            texter.text = "" + timeling;
            realTime += Time.deltaTime;
            if (realTime >= 1)
            {
                timeling--;
                realTime -= 1;
            }
        }
    }
}
