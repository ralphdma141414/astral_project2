using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheCore : MonoBehaviour
{
    private Scene currentScene;
    public string sceneName, lastName;
    public AudioSource musique, nonLoopMusique;
    public AudioClip[] music, trigMusic;
    private bool change = true, playable = true, loading = true, triggered = true;
    public PlayerAlive seePlayer;
    [HideInInspector]
    public float volume = 1f;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
    }

    // Update is called once per frame
    void Update()
    {
        SceneIdentify();
        ChangedScene();
        if (playable == true && loading == false)
        {
            seePlayer.FindPlayer();
            Playing();
        }
        else
        {
            NonPlaying();
        }
    }

    void SceneIdentify()
    {
        AudioListener.volume = volume;
        currentScene = SceneManager.GetActiveScene();
        if (change == true && loading == true)
        {
            if (sceneName == "Level1" || sceneName == "Level1Copia")
            {
                Playable(1);
            }
            else if (sceneName == "Level2")
            {
                Playable(2);
            }
            else if (sceneName == "Level3")
            {
                Playable(3);
            }

            else if (sceneName == "LevelBoss")
            {
                Playable(4);
            }
            else if (sceneName == "TestScene")
            {
                Playable(1);
            }
            else if (sceneName == "WinScene")
            {
                EndedTrigger(1);
            }
            else if (sceneName == "LoseScene")
            {
                EndedTrigger(2);
            }
            else
            {
                NonPlayable();
            }
            change = false;
        }
    }

    void Playable(int number)
    {
        playable = true;
        nonLoopMusique.Stop();
        musique.Stop();
        musique.clip = music[number - 1];
        MusicPlay();
        lastName = sceneName;
        loading = false;
        seePlayer.nonPlaying = false;
    }

    void NonPlayable()
    {
        if (playable||triggered)
        {
            nonLoopMusique.Stop();
            musique.Stop();
        }
        playable = false;
        triggered = false;

    }

    void EndedTrigger(int number)
    {
        if (playable)
        {
            triggered = true;
            musique.Stop();
            nonLoopMusique.clip = trigMusic[number - 1];
            nonLoopMusique.Play();
        }
        playable = false;

    }

    void Playing()
    {
        Cursor.visible = false;
        if (seePlayer.alive == false && loading == false)
        {
            SceneManager.LoadScene("LoseScene");
        }
    }
    void NonPlaying()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    void ChangedScene()
    {
        if (sceneName != currentScene.name)
        {
            loading = true;
            sceneName = currentScene.name;
            if (playable == true)
            {
                seePlayer.Changing();
            }
            change = true;
        }
    }
    void MusicPlay()
    {
        musique.Play();
    }
    public void Restarting()
    {
        SceneManager.LoadScene(lastName);
        sceneName = "000000";
        seePlayer.nonPlaying = true;
    }
}