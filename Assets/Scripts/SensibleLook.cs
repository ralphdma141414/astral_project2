using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensibleLook : MonoBehaviour
{
    private InputField aidale;
    private int numberSens;
    private GameObject gaming;
    private PlayerAlive lor;
    public Slider sliderSens;
    // Start is called before the first frame update
    void Start()
    {
        gaming = GameObject.FindGameObjectWithTag("The Core");
        lor = gaming.GetComponent<PlayerAlive>();
        aidale = GetComponent<InputField>();
        aidale.text = "" + lor.sers + "";
        sliderSens.minValue = 30;
        sliderSens.maxValue = 150;

        sliderSens.onValueChanged.AddListener((v) =>
        {
            aidale.text = v.ToString("0");
        });
        sliderSens.value = lor.sers;

    }

    // Update is called once per frame
    void Update()
    {
        Selecting();
    }

    void Selecting()
    {
        lor.sers = numberSens;

        if (int.TryParse(aidale.text, out numberSens))
        {
            aidale.text = "" + numberSens + "";

        }
        if (Input.GetKeyDown(KeyCode.Return)|| Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (numberSens > 150)
            {
                numberSens = 150;
                aidale.text = "" + numberSens + "";

            }
            else if (numberSens < 30)
            {
                numberSens = 30;
                aidale.text = "" + numberSens + "";

            }
            sliderSens.value = lor.sers;

        }

    }
}
