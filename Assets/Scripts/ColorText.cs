using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorText : MonoBehaviour
{
    public Text[] texter;
    public void ColorButton(int nonLook)
    {
        for(int i = 0; i < texter.Length; i++)
        {
            if (nonLook != i)
            {
                texter[i].color = Color.black;
            }
        }
    }
    public void ColorRevert()
    {
        for (int i = 0; i < texter.Length; i++)
        {
            texter[i].color = Color.white;
        }
    }

}
