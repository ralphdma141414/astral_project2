using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveButton : MonoBehaviour
{
    public Doorman doorey;
    private bool available;
    public SoundCreate sound;
    public Material emiss;

    private void Start()
    {
        emiss.EnableKeyword("_EMISSION");
        emiss.SetColor("_EmissionColor", new Color(255, 0, 0) * 0.015f);
    }

    void Update()
    {
        Pressing();
    }

    void Pressing()
    {
        if (available && Input.GetKeyDown(KeyCode.E))
        {
            doorey.Opening();
            sound.Sounding();
            emiss.SetColor("_EmissionColor", new Color(0, 255, 0) * 0.017f);
        }
    }
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Interect"))
        {
            available = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Interect"))
        {
            available = false;
        }
    }
}
