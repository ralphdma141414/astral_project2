using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    private int number;
    public Text texter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        NumberWard();
    }

    void NumberWard()
    {
        switch (number)
        {
            case 1:
                texter.enabled = true;
                texter.text = "Esa puerta se ve fragil, creo que puedo dispararle (Click izquierso)";
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    number = 0;
                }
                break;
            case 2:
                texter.enabled = true;
                texter.text = "Creo que si voy rapido puedo cruzarlo (Shift)";
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    number = 0;
                }
                break;

            case 3:
                texter.enabled = true;
                texter.text = "Ese se ve peligroso, quizas tengo que matarlo (Click izquierdo)";
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    number = 0;
                }
                break;

            default:
                texter.enabled = false;
                texter.text = "";
                break;
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("TutorialTrain"))
        {
            number = other.gameObject.GetComponent<TutorialNumber>().number;
            Destroy(other.gameObject);
        }
    }
}
