﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artillery : MonoBehaviour
{
    public GameObject target, dot, bullet, turret;
    public EntityValues alerte;
    public float rotation, timer = 0;
    public Vector3 look, targetO, turretO;
    [HideInInspector]
    public Detector deter;
    public EnemyRecoiler looker;
    public bool isShooting;
    public SoundCreate shoot, death;
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (alerte.currentLife > 0)
        {
            if (target != null)
            {
                Looking();
                Shooting();
            }
        }
        else
        {
            Death();
        }
    }

    void Looking()
    {
        if (deter.dosto)
        {
            targetO = target.transform.position;
            targetO.y = 0;
            turretO = turret.transform.position;
            turretO.y = 0;
            look = (targetO - turretO).normalized;
            rotation = Mathf.Atan2(look.x, look.z);
            rotation = rotation * (180 / Mathf.PI);
            turret.transform.localEulerAngles = new Vector3(0, rotation, 0);
        }
    }

    void Shooting()
    {
        timer += Time.deltaTime;
        if (timer >= 1 && deter.dosto && looker.detect)
        {
            shoot.Sounding();
            isShooting = true;
            bullet.transform.position = dot.transform.position;
            bullet.transform.localEulerAngles = turret.transform.localEulerAngles;
            bullet.GetComponent<Bullet>().vel = look*7;
            bullet.GetComponent<LifeSpan>().maxTimer = 5;
            bullet.tag = "Enemy bullet";
            Instantiate(bullet);
            timer = 0;
            shoot.count = 0;
        }
        else
        {
            isShooting = false;
        }
    }

    void Death()
    {
        death.Sounding();
    }
    void VolumeRate()
    {
        if (looker.detect)
        {
            shoot.sound.volume = 1f;
            death.sound.volume = 1f;
        }
        else
        {
            shoot.sound.volume = 0.5f;
            death.sound.volume = 0.5f;
        }
    }
}
