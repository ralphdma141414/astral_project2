using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recoiler : MonoBehaviour
{
    public Transform cam, rotationOrigin;
    private RaycastHit hitWall, hitEnemy;
    private Vector3 obj;
    private float tryWall, tryEnemy;
    private bool enemy = true, wall = true;
    public Image pointer;
    public Sprite looked, nonLooked;
    [HideInInspector]
    public bool dead;

    // Start is called before the first frame update
    void Start()
    {
        obj = cam.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (dead == false)
        {
            Recoiling();
        }
    }
    void Recoiling()
    {
        LayerMask mask = LayerMask.GetMask("FloorAndWall");
        LayerMask enemyLook = LayerMask.GetMask("Enemy");
        if (Physics.Linecast(transform.position, transform.position + rotationOrigin.transform.localRotation * obj, out hitEnemy, enemyLook))
        {
            enemy = true;
            tryEnemy = Vector3.Distance(transform.position, hitEnemy.point);
        }
        else
        {
            enemy = false;
            tryEnemy = 1000000;
        }

        if (Physics.Linecast(transform.position, transform.position + rotationOrigin.transform.localRotation * obj, out hitWall, mask))
        {
            wall = true;
            tryWall = Vector3.Distance(transform.position, hitWall.point);
        }
        else
        {
            wall = false;
            tryWall = 1000000;
        }
        if ((tryWall <= tryEnemy) && (enemy == true || wall == true))
        {
            pointer.sprite = nonLooked;
            cam.localPosition = new Vector3(0, 0, Vector3.Distance(transform.position, hitWall.point));
        }
        else if ((tryWall > tryEnemy) && (enemy == true || wall == true))
        {
            pointer.sprite = looked;
            cam.localPosition = new Vector3(0, 0, Vector3.Distance(transform.position, hitEnemy.point));
        }
        else if (wall == false && enemy == false)
        {
            pointer.sprite = nonLooked;
            cam.localPosition = Vector3.Lerp(cam.localPosition, obj, Time.deltaTime);
        }
    }
}

