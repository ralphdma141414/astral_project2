using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheCoreOneTime : MonoBehaviour
{
    private GameObject gaming;
    public GameObject gameEntering;
    // Start is called before the first frame update
    void Start()
    {
        gaming = GameObject.FindGameObjectWithTag("The Core");
        if (gaming == null)
        {
            Instantiate(gameEntering);
        }
    }
}
