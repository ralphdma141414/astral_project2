﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MeleeBot : MonoBehaviour
{
    [HideInInspector]
    public NavMeshAgent agent;
    public GameObject target, temporal, tesla, postion;
    private Rigidbody rigid;
    public int chargeCount = 0, teslaCount=0;
    [HideInInspector]
    public bool spawned, active = false;
    public bool charging = false, rest = false, preparing = false;
    public float rotation, timer, restTimer, speedRem, preTime;
    public EntityValues alerte;
    public Vector3 look;
    public Detector deter, touch, rage, pond;
    public EnemyRecoiler looker;
    public SoundCreate bash, wakingUp, death;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player");
        Physics.IgnoreCollision(target.GetComponent<Collider>(), GetComponent<Collider>());
        rigid = GetComponent<Rigidbody>();
        speedRem = agent.speed;
    }

    void Update()
    {
        if (alerte.currentLife > 0)
        {
            if (active == false)
            {
                Sleeping();
            }
            else if (active && target != null)
            {
                if (rest == false && charging == false)
                {
                    Looking();
                    Chasing();
                }
                else if (charging && preparing && rest == false)
                {
                    ChargeLoad();
                    Looking();
                }
                else if (charging && rest == false && preparing == false)
                {
                    Charge();
                    Attack();
                }
                else if (rest)
                {
                    Resting();

                }
            }
            VolumeRate();
        }
        else
        {
            DeadRate();
        }
    }

    void Sleeping()
    {
        if (((deter.dosto || alerte.damaged == true) && looker.detect) || spawned)
        {
            teslaCount = 0;
            active = true;
            Destroy(temporal);
            //Debug.Log("Tan tan");
        }
    }

    void Looking()
    {
        chargeCount = 0;
        look = (target.transform.position - transform.position).normalized;
        rotation = Mathf.Atan2(look.x, look.z);
        rotation = rotation * (180 / Mathf.PI);
        transform.localEulerAngles = new Vector3(0, rotation, 0);
    }

    void Chasing()
    {
        agent.speed = speedRem;
        timer += Time.deltaTime;
        agent.SetDestination(postion.transform.position);

        if (rage.dosto)
        {
            charging = true;
            preparing = true; //Animación de preparar ataque
            bash.Sounding();
        }
    }
    void ChargeLoad()
    {
        preTime += Time.deltaTime;
        rigid.velocity = new Vector3(0, 0, 0);
        agent.isStopped = true;
        agent.ResetPath();
        if (preTime >= 1.5f)
        {
            preparing = false;
            preTime = 0;
            bash.count = 0;
            //Debug.Log("Bufffffff");
        }
    }

    void Charge()
    {
        charging = true;
        if (chargeCount == 0)
        {
            Vector3 vel = rigid.velocity;
            vel.x = look.x * agent.speed * 2;
            vel.z = look.z * agent.speed * 2;
            vel.y = 0;
            rigid.velocity = vel;
            chargeCount++;

        }
        agent.SetDestination(postion.transform.position);
        if (pond.dosto || touch.dosto)
        {
            rest = true;
            chargeCount = 0;
            charging = false;
            Attack();
            
        }
    }

    void Attack()
    {
        if (touch.dosto)
        {
            if (teslaCount == 0)
            {
                tesla.transform.position = transform.position;
                Instantiate(tesla);
                teslaCount++;
                timer = 0;
                //Debug.Log("atack");
            }
        }
    }
    void Resting()
    {
        teslaCount = 0;
        rigid.velocity = new Vector3(0, 0, 0);
        agent.isStopped = true;
        agent.ResetPath();
        restTimer += Time.deltaTime;
        if (restTimer >= 3)
        {
            restTimer = 0;
            rest = false;
        }
    }
    void DeadRate()
    {
        rigid.velocity = new Vector3(0, 0, 0);
        agent.isStopped = true;
        agent.ResetPath();
        death.Sounding();
    }
    void VolumeRate()
    {
        if (looker.detect)
        {
            bash.sound.volume = 1f;
            //wakingUp.sound.volume = 1f;
            death.sound.volume = 1f;
        }
        else
        {
            bash.sound.volume = 0.5f;
            //wakingUp.sound.volume = 0.5f;
            death.sound.volume = 0.5f;
        }
    }
}
