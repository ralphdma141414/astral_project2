using UnityEngine.AI;
using UnityEngine;

public class EnemyMele : MonoBehaviour
{
    [Header("Refferences")]
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Transform target;
    [SerializeField] private SkinnedMeshRenderer skinnedRenderer;
    [SerializeField] private Animator animator;
    [Header("Vida Settings")]
    [SerializeField] private Life vida;
    [Header("Settings")]
    [Tooltip("Distancia para atacar")] public float chasingDistance = 2f;
    [Tooltip("Tiempo de espera antes de atacar")] public float chasingRate;
    [Tooltip("Tiempo de espera antes de descansar")] public float attackRate;

    private bool isSleeping = true;
    private Vector3 sleepingPosition;
    private Vector3 followPosition;
    private Vector3 distanceVector;
    private float chasingTimer;
    private float attackTimer;
    private bool canDamagePlayer;

    private void Awake()
    {
        sleepingPosition = transform.position;
    }

    //Sleep => FollowTarget => Charge => Attack => Rest => FollowTarget (loop)
    void FixedUpdate()
    {
        followPosition = isSleeping ? Sleeping() : ChasingPlayer();
        animator.SetFloat("Speed", agent.velocity.magnitude);
        agent.SetDestination(followPosition);
    }

    private Vector3 ChasingPlayer()
    {
        animator.SetTrigger("WakeUp");

        distanceVector = transform.position - target.position;

        //Debug.Log($"chasing timer {chasingTimer} attack timer {attackTimer}");

        chasingTimer = Mathf.Clamp(chasingTimer, 0, chasingRate);
        attackTimer = Mathf.Clamp(attackTimer, 0, attackRate);

        if (distanceVector.magnitude < chasingDistance)
        {
            chasingTimer += Time.deltaTime; 
            ChargeAttack(chasingTimer>=chasingRate); 
        }
        else
        {
            chasingTimer -= chasingRate;
            ChargeAttack(false);
        }

        return target.position;
    }

    private void ChargeAttack(bool canAttack)
    {
        if (canAttack) 
        {
            skinnedRenderer.material.color = Color.red;
            attackTimer += Time.deltaTime;  
            Attack(attackTimer >= attackRate); 
        }
        else
        {
            attackTimer -= attackRate;
            skinnedRenderer.material.color = Color.white;
            Attack(false);
        }
    }

    private void Attack(bool canAttack) 
    {
        canDamagePlayer = canAttack;

        if (canAttack)
        {
            animator.SetTrigger("Attack");
            chasingTimer -= chasingRate; 
        }
    }

    private Vector3 Sleeping()
    {
        var sleepingDistance = transform.position - sleepingPosition;

        if (sleepingDistance.magnitude < 0.1f)
        {
            animator.SetTrigger("Sleep");
        }
        return sleepingPosition;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (canDamagePlayer && collision.transform.CompareTag("Player"))
        {
           // collision.transform.GetComponent<PlayerMovement>().life.RecibirAtaque(100);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isSleeping = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isSleeping = true;
        }
    }
}

