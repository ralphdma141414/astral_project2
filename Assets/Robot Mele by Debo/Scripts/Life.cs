using System;
using UnityEngine.UI;

[Serializable]
public class Life
{
    public float vidaTotal;
    private float vidaActual;
    public Image imagenVida;

    public bool IsAlive { get; private set; } //se pueda leer, no escribir

    public void Init()
    {
        IsAlive = true;
        vidaTotal = vidaActual;
    }

    public void RecibirAtaque(float damage)
    {
        vidaActual -= damage;
        imagenVida.fillAmount = vidaActual / vidaTotal;

        if (vidaActual <= 0)
        {
            IsAlive = false;
        }
    }
}
