using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoAnim : MonoBehaviour
{
    private Animator anim;
    public Artillery rangoBot;
    private EntityValues entityRango;
    public Material emiss;
    public ParticleSystem particles;

    void Start()
    {
        anim = GetComponent<Animator>();
        entityRango = GetComponentInParent<EntityValues>();
        emiss.EnableKeyword("_EMISSION");
    }

    void Update()
    {
        if (rangoBot.deter.dosto)
        {
            anim.SetBool("Detect", true);
        }
        else
        {
            anim.SetBool("Detect", false);
        }

        if (rangoBot.isShooting)
        {
            anim.SetBool("Shoot", true);
        }
        else
        {
            anim.SetBool("Shoot", false);
        }

        if (entityRango.currentLife <= 0)
        {
            if (!particles.isPlaying)
            {
                particles.Play();
            }
            anim.SetBool("Shoot", false);
            anim.SetBool("Detect", false);
            anim.SetBool("Dead", true);
        }
        else
        {
            anim.SetBool("Dead", false);
        }

        //Cambio de color
        if (entityRango.deadTime >= 1f)
        {
            emiss.SetColor("_EmissionColor", new Color(0, 0, 0, 0));
        }
        else
        {
            emiss.SetColor("_EmissionColor", new Color(191, 43, 0) * 0.018f);
        }
    }
}
