using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnim : MonoBehaviour
{
    private Animator anim;
    private EntityValues entityHP;
    public Material emiss;
    void Start()
    {
        anim = GetComponent<Animator>();
        entityHP = GetComponentInParent<EntityValues>();
        emiss.EnableKeyword("_EMISSION");
    }

    void Update()
    {
        anim.SetFloat("Horizontal", Input.GetAxisRaw("Horizontal"), 0.2f, Time.deltaTime);
        anim.SetFloat("Vertical", Input.GetAxisRaw("Vertical"), 0.2f, Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetTrigger("Dash");
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("Shoot", true);
        }
        else
        {
            anim.SetBool("Shoot", false);
        }

        if (entityHP.currentLife <= 0)
        {
            anim.SetBool("Dead", true);
        }
        else
        {
            anim.SetBool("Dead", false);
        }

        //Cambio de color en las alas
        if (entityHP.currentLife >= 50)
        {
            emiss.SetColor("_EmissionColor", new Color(28, 42, 191) * 0.015f);
        }
        else if (entityHP.currentLife <= 40)
        {
            emiss.SetColor("_EmissionColor", new Color(152, 28, 106) * 0.015f);
        }
    }
}
