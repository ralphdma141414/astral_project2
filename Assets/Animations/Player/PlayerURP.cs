using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;

public class PlayerURP : MonoBehaviour
{
    [SerializeField] private Vignette vignette;
    [SerializeField] private PostProcessVolume volume;
    //[SerializeField] private Volume volume;
    [SerializeField] private EntityValues entityHP;

    void Start()
    {
        volume = GetComponent<PostProcessVolume>();
        //volume = GetComponent<Volume>();
        //Debug.Log(volume);
        volume.profile.TryGetSettings(out vignette);
        entityHP = GetComponentInParent<EntityValues>();
        //Debug.Log(vignette.color);
    }

    void Update()
    {
        if (entityHP.currentLife <= 30)
        {
            vignette.color.value = new Color(255, 0, 0);
        }
    }
}
