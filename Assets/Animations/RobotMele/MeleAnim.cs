using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleAnim : MonoBehaviour
{
    private Animator anim;
    public MeleeBot meleBot;
    public EntityValues entityMele;
    public ParticleSystem particles;

    void Start()
    {
        anim = GetComponent<Animator>();
        entityMele = GetComponentInParent<EntityValues>();
    }

    void Update()
    {
        if (meleBot.active)
        {
            anim.SetBool("Detect", true);
        }
        else
        {
            anim.SetBool("Detect", false);
        }

        if (meleBot.agent.speed == meleBot.speedRem)
        {
            anim.SetBool("Walk", true);
            if (meleBot.preparing || meleBot.charging)
            {
                anim.SetBool("Walk", false);
            }
        }
        else
        {
            anim.SetBool("Walk", false);
        }

        if (meleBot.touch.dosto)
        {
            anim.SetBool("Walk", false);
            anim.SetBool("Atack", true);
        }
        else
        {
            anim.SetBool("Atack", false);
        }

        if (entityMele.currentLife <= 0)
        {
            if (!particles.isPlaying)
            {
                particles.Play();
            }
            anim.SetBool("Walk", false);
            anim.SetBool("Dead", true);
        }
    }
}
